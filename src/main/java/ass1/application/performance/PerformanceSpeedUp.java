package ass1.application.performance;

import ass1.model.computation.ForceComputation2D;
import ass1.model.particle.Particle2D;
import ass1.model.particle.ParticleSystem;
import ass1.model.particle.Vector2D;
import ass1.model.simulation.SimulationBuilder;
import ass1.model.simulation.SimulationController;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PerformanceSpeedUp {

    private static final int NUMBER_OF_PARTICLES = 1000;
    private static final double K = 1;
    private static final double FRICTION = 0;

    private static final int STEPS = 1000;
    private static final double DELTA_TIME = 0.01d;

    public static void main(String... args) {

        final ParticleSystem<Vector2D> particleSystem1 = new ParticleSystem<>(
                IntStream.range(0, NUMBER_OF_PARTICLES)
                        .boxed()
                        .map(i -> Particle2D.randomPosition(i, 1, 1))
                        .collect(Collectors.toList()),
                new ForceComputation2D(),
                K,
                FRICTION
        );
        final SimulationController sequentialSimulation = SimulationBuilder.sequential(particleSystem1)
                .setSteps(STEPS)
                .setDeltaTime(DELTA_TIME)
                .build();

        Chrono chrono = new Chrono();

        chrono.start();
        sequentialSimulation.startSimulation();
        sequentialSimulation.waitSimulation();
        chrono.stop();
        final long timeSequential = chrono.getTime();

        System.out.println("Performance result:");
        System.out.println("\tSequential: " + timeSequential + "[ms]");

        // test with different number of thread.
        for(int currentThreads = 2; currentThreads <= Runtime.getRuntime().availableProcessors(); currentThreads++) {

            final SimulationController sim = SimulationBuilder.parallel(particleSystem1)
                    .setNumberOfThread(currentThreads)
                    .setSteps(STEPS)
                    .setDeltaTime(DELTA_TIME)
                    .build();

            chrono.start();
            sim.startSimulation();
            sim.waitSimulation();
            chrono.stop();
            final long timeParallel = chrono.getTime();

            System.out.println("\tParallel with " + currentThreads + " threads:\n" +
                    "\tTime:" + timeParallel + "[ms]" +
                    "\tSpeedUp: " + (timeSequential / (double)timeParallel));
        }
    }
}
