package ass1.application;

import ass1.model.computation.ForceComputation2D;
import ass1.model.particle.Particle2D;
import ass1.model.particle.ParticleSystem;
import ass1.model.particle.Vector2D;
import ass1.model.simulation.SimulationBuilder;
import ass1.model.simulation.SimulationController;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SameResultTest {

    public static void main(String... args) {

        final ParticleSystem<Vector2D> particleSystem1 = new ParticleSystem<>(
                IntStream.range(0, 300)
                        .boxed()
                        .map(i -> Particle2D.randomPosition(i, 1, 1)).collect(Collectors.toList()),
                new ForceComputation2D(),
                1,
                0
        );

        final ParticleSystem<Vector2D> particleSystem2 = particleSystem1.deepCopy();

        final SimulationController sequentialSimulation = SimulationBuilder.sequential(particleSystem1)
                .setSteps(1000)
                .setDeltaTime(0.01d)
                .build();

        final SimulationController paralellSimulation = SimulationBuilder.parallel(particleSystem2)
                .setNumberOfThread(Runtime.getRuntime().availableProcessors() / 2 + 1)
                .setSteps(1000)
                .setDeltaTime(0.01d)
                .build();

        sequentialSimulation.startSimulation();
        sequentialSimulation.waitSimulation();
        paralellSimulation.startSimulation();
        paralellSimulation.waitSimulation();

        System.out.println("Sequential result: ");
        System.out.println(particleSystem1);

        System.out.println("Parallel result: ");
        System.out.println(particleSystem2);

        System.out.println("Same result: " + particleSystem1.equals(particleSystem2));
    }

}
