package ass1.application;

import ass1.gui.CoordinateMapper;
import ass1.gui.FPSDelay;
import ass1.gui.swing.SwingParticle2D;
import ass1.gui.drawer.ParticleDrawer;
import ass1.model.computation.ForceComputation2D;
import ass1.model.particle.Particle;
import ass1.model.particle.Particle2D;
import ass1.model.particle.ParticleSystem;
import ass1.model.particle.Vector2D;
import ass1.model.simulation.SimulationBuilder;
import ass1.model.simulation.SimulationController;
import ass1.model.simulation.parallel.ParallelSimulationController;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ElettroSimulation {

    private static final int WIDTH = 1280;
    private static final int HEIGHT = 800;

    private static final int NUMBER_OF_ELECTRON = 250;
    private static final int NUMBER_OF_PROTON = 250;

    private static final int STEPS = 2000;
    private static final double DT = 0.0000000001d;
    private static final double K = 8.98e19;
    private static final double FRICTION = 0;

    private static final double ELECTRON_CARICA = -1.60e-19;
    private static final double PROTON_CARICA = 1.60e-19;

    private static final double ELECTRON_MASS = 9.109e-31;
    private static final double PROTON_MASS = 1.673e-27;

    public static void main(String... args) {

        final List<Particle<Vector2D>> electrons = IntStream.range(0, NUMBER_OF_ELECTRON)
                .boxed()
                .map(i -> Particle2D.randomPosition(i, ELECTRON_MASS, ELECTRON_CARICA))
                .collect(Collectors.toList());

        final List<Particle<Vector2D>> protons = IntStream.range(0, NUMBER_OF_PROTON)
                .boxed()
                .map(i -> Particle2D.randomPosition(i + NUMBER_OF_ELECTRON, PROTON_MASS, PROTON_CARICA))
                .collect(Collectors.toList());


        final ParticleSystem<Vector2D> particleSystem = new ParticleSystem<>(Stream.concat(electrons.stream(), protons.stream()).collect(Collectors.toList()),
                new ForceComputation2D(),
                K,
                FRICTION);

        final SimulationController simulation = SimulationBuilder.parallel(particleSystem)
                .setSteps(STEPS)
                .setDeltaTime(DT)
                .setSimulationDelay(FPSDelay.targetFPS(60))
                .build();

        final CoordinateMapper<Vector2D> mapper = CoordinateMapper.maxRange2D(WIDTH / 3d, HEIGHT / 3d);
        final ParticleDrawer<Graphics2D, Vector2D> drawer = (drawer1, particle) -> {
            if(particle.getAlpha() > 0)
                drawer1.setColor(Color.RED);
            else
                drawer1.setColor(Color.BLUE);

            final Vector2D mapped = mapper.map(particle.getPosition());
            drawer1.fillOval((int) mapped.getX(),
                    (int) mapped.getY(),
                    8, 8);
        };

        final SwingParticle2D view = SwingParticle2D.fromSimulation(simulation, drawer, WIDTH, HEIGHT);
        particleSystem.attach(view);

        view.setVisible(true);
    }
}
