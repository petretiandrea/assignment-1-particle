package ass1.application;

import ass1.gui.CoordinateMapper;
import ass1.gui.FPSDelay;
import ass1.gui.drawer.ParticleDrawer;
import ass1.gui.swing.SwingParticle2D;
import ass1.model.computation.ForceComputation2D;
import ass1.model.particle.Particle2D;
import ass1.model.particle.ParticleSystem;
import ass1.model.particle.Vector2D;
import ass1.model.simulation.SimulationBuilder;
import ass1.model.simulation.SimulationController;

import java.awt.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StandardSimulation {

    private static final int WIDTH = 800;
    private static final int HEIGHT = 800;

    public static void main(String... args) {
        final ParticleSystem<Vector2D> particleSystem = new ParticleSystem<>(
                IntStream.range(0, 1000)
                        .boxed()
                        .map(i -> Particle2D.randomPosition(i, 1, 1))
                        .collect(Collectors.toList()),
                new ForceComputation2D(),
                1,
                0
        );

        final SimulationController simulation = SimulationBuilder.parallel(particleSystem)
                .setNumberOfThread(Runtime.getRuntime().availableProcessors() / 2 + 1)
                .setSteps(1000)
                .setDeltaTime(0.0001d)
                .setSimulationDelay(FPSDelay.targetFPS(60))
                .build();

        final CoordinateMapper<Vector2D> mapper = CoordinateMapper.maxRange2D(WIDTH / 3d, HEIGHT / 3d);
        final ParticleDrawer<Graphics2D, Vector2D> drawer = (drawer1, particle) -> {
            final Vector2D mapped = mapper.map(particle.getPosition());
            drawer1.fillRect((int) mapped.getX(),
                    (int) mapped.getY(),
                    5, 5);
        };

        final SwingParticle2D view = SwingParticle2D.fromSimulation(simulation, drawer, WIDTH, HEIGHT);
        particleSystem.attach(view);
        view.setVisible(true);
    }

}
