package ass1.utils;

public class Log {

    private Log() {}

    public synchronized static void log(String msg) {
        System.out.println("[" + Thread.currentThread().getName() + "]: " + msg);
    }

}
