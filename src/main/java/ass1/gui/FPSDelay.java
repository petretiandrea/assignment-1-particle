package ass1.gui;

import java.util.function.Consumer;

public final class FPSDelay {

    public static Consumer<Long> targetFPS(final int fps) {
        final long targetTime = (long) 1000d / fps;
        return dt -> {
            final long waitTime = targetTime - dt;
            if(waitTime > 0) {
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static Consumer<Long> busyWaitTargetFPS(final int fps) {
        throw new IllegalAccessError();
    }
}
