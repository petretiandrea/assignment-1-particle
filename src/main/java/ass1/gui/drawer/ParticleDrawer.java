package ass1.gui.drawer;

import ass1.model.particle.Particle;

public interface ParticleDrawer<C, D> {
    void draw(C drawer, Particle<D> particle);
}
