package ass1.gui.swing;

import ass1.gui.drawer.ParticleDrawer;
import ass1.model.particle.ParticleSystem;
import ass1.model.particle.ParticleSystemObserver;
import ass1.model.particle.Vector2D;
import ass1.model.simulation.SimulationController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class SwingParticle2D extends JFrame implements ParticleSystemObserver<Vector2D> {

    private ParticleView particleView;
    private SimulationController simulationController;

    public static SwingParticle2D fromSimulation(final SimulationController simulationController,
                                                 final ParticleDrawer<Graphics2D, Vector2D> drawer,
                                                 final int width,
                                                 final int height)
    {
        return new SwingParticle2D(simulationController, drawer, width, height);
    }

    private SwingParticle2D(final SimulationController simulation, final ParticleDrawer<Graphics2D, Vector2D> drawer, int width, int height) {
        setSize(width, height);
        particleView = new ParticleView(drawer);
        simulationController = simulation;

        initGuiComponents();
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev){
                System.exit(-1);
            }
        });
        setResizable(false);
    }

    private void initGuiComponents() {
        JButton buttonStart = new JButton("Start");
        JButton buttonStop = new JButton("Stop");

        JPanel jPanel = new JPanel();
        jPanel.add(buttonStart);
        jPanel.add(buttonStop);

        buttonStart.addActionListener(e -> simulationController.startSimulation());
        buttonStop.addActionListener(e -> simulationController.stopSimulation());

        setLayout(new BorderLayout());
        add(jPanel, BorderLayout.NORTH);
        add(particleView, BorderLayout.CENTER);
    }

    @Override
    public void onUpdateModel(ParticleSystem<Vector2D> particleSystem) {
        particleView.updateRender(new ArrayList<>(particleSystem.getCurrentParticles()));
    }
}
