package ass1.gui.swing;

import ass1.gui.drawer.ParticleDrawer;
import ass1.model.particle.Particle;
import ass1.model.particle.ParticleSystem;
import ass1.model.particle.Vector2D;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Objects;

public class ParticleView extends JPanel {

    private List<Particle<Vector2D>> toRender;
    private final ParticleDrawer<Graphics2D, Vector2D> particleDrawer;

    public ParticleView(final ParticleDrawer<Graphics2D, Vector2D> particleDrawer) {
        this.particleDrawer = particleDrawer;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g2.clearRect(0,0,this.getWidth(),this.getHeight());

        // render the list of particle using particle drawer
        synchronized (this) {
            if(Objects.nonNull(toRender)) {
                toRender.forEach(p -> particleDrawer.draw(g2, p));
            }
        }
    }

    public void updateRender(final List<Particle<Vector2D>> particles) {
        synchronized (this) {
            this.toRender = particles;
        }
        SwingUtilities.invokeLater(this::repaint);
    }
}
