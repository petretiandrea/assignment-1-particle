package ass1.gui;

import ass1.model.particle.Vector2D;

public abstract class CoordinateMapper<V> {

    public static CoordinateMapper<Vector2D> maxRange2D(double maxX, double maxY) {
        return new MaxRange(maxX, maxY);
    }

    public abstract V map(V point);

    private static class MaxRange extends CoordinateMapper<Vector2D>{

        private final double maxX;
        private final double maxY;

        MaxRange(double maxX, double maxY) {
            this.maxX = maxX;
            this.maxY = maxY;
        }

        @Override
        public Vector2D map(Vector2D point) {
            return new Vector2D(map(point.getX(), maxX), map(point.getY(), maxY));
        }

        private double map(double value, double maxValue) {
            return (value * maxValue + maxValue / 2);
        }
    }

    // private double map(double x, double inMin, double inMax, double outMin, double outMax) {
    //            return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    //        }
}
