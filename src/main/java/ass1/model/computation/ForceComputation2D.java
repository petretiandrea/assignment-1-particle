package ass1.model.computation;

import ass1.model.particle.Particle;
import ass1.model.particle.Vector2D;

import java.util.List;

public class ForceComputation2D implements ForceComputationStrategy<Vector2D> {

    @Override
    public Vector2D computeForce(List<Particle<Vector2D>> particles, Particle<Vector2D> particle, double k, double friction) {
        final Vector2D force = Vector2D.zero();
        for (Particle<Vector2D> p : particles) {
            if (!p.equals(particle)) {
                double dx = particle.getPosition().getX() - p.getPosition().getX();
                double dy = particle.getPosition().getY() - p.getPosition().getY();
                final double distance = Math.sqrt(dx * dx + dy * dy);
                final double num = k * particle.getAlpha() * p.getAlpha() / (Math.pow(distance, 3));
                final double attrX = -friction * p.getVelocity().getX();
                final double attrY = -friction * p.getVelocity().getY();
                force.setTo(force.getX() + (num * dx) + attrX, force.getY() + (num * dy) + attrY);
            }
        }
        return force;
    }
}
