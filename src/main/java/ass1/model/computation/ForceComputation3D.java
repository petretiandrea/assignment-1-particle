package ass1.model.computation;

import ass1.model.particle.Particle;
import ass1.model.particle.Vector3D;

import java.util.List;

public class ForceComputation3D implements ForceComputationStrategy<Vector3D> {

    @Override
    public Vector3D computeForce(List<Particle<Vector3D>> particles, Particle<Vector3D> particle, double k, double friction) {
        final  Vector3D force = Vector3D.zero();
        for (Particle<Vector3D> p : particles) {
            if (!p.equals(particle)) {
                double dx = particle.getPosition().getX() - p.getPosition().getX();
                double dy = particle.getPosition().getY() - p.getPosition().getY();
                double dz = particle.getPosition().getZ() - p.getPosition().getZ();
                final double distance = Math.sqrt(dx * dx + dy * dy + dz * dz);

                final double num = k * particle.getAlpha() * p.getAlpha() / (Math.pow(distance, 3));
                final double attrX = -friction * p.getVelocity().getX();
                final double attrY = -friction * p.getVelocity().getY();
                final double attrZ = -friction * p.getVelocity().getZ();

                force.setTo(force.getX() + (num * dx) + attrX,
                        force.getY() + (num * dy) + attrY,
                        force.getZ() + (num * dz) + attrZ);
            }
        }
        return force;
    }
}
