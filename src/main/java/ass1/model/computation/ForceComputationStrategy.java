package ass1.model.computation;

import ass1.model.particle.Particle;

import java.util.List;

public interface ForceComputationStrategy<D> {
    D computeForce(final List<Particle<D>> particles, final Particle<D> particle, final double k, final double friction);
}
