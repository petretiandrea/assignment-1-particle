package ass1.model.simulation;

import ass1.model.particle.ParticleSystem;
import ass1.model.simulation.parallel.ParallelSimulationBuilder;
import ass1.model.simulation.sequential.SequentialSimulationBuilder;

import java.util.function.Consumer;

public abstract class SimulationBuilder<D> {

    public static <D> SequentialSimulationBuilder<D> sequential(final ParticleSystem<D> particleSystem) {
        return new SequentialSimulationBuilder<>(particleSystem);
    }

    public static <D> ParallelSimulationBuilder<D> parallel(final ParticleSystem<D> particleSystem) {
        return new ParallelSimulationBuilder<>(particleSystem);
    }

    private ParticleSystem<D> particleSystem;

    private Consumer<Long> simulationDelay;
    private int nSteps;
    private double dt;

    protected SimulationBuilder(final ParticleSystem<D> particleSystem) {
        this.particleSystem = particleSystem;
        this.nSteps = 0;
        this.dt = 0;
        this.simulationDelay = e -> {};
    }

    public SimulationBuilder<D> setSteps(final int steps){
        this.nSteps = steps;
        return this;
    }

    public SimulationBuilder<D> setDeltaTime(final double deltaTime) {
        this.dt = deltaTime;
        return this;
    }

    public SimulationBuilder<D> setSimulationDelay(final Consumer<Long> simulationDelay) {
        this.simulationDelay = simulationDelay;
        return this;
    }

    public abstract SimulationController build();

    public ParticleSystem<D> getParticleSystem() {
        return particleSystem;
    }

    protected Consumer<Long> getSimulationDelay() {
        return simulationDelay;
    }

    protected int getSteps() {
        return nSteps;
    }

    protected double getDeltaTime() {
        return dt;
    }
}
