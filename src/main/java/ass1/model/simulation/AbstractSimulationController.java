package ass1.model.simulation;

import ass1.model.Flag;
import ass1.model.particle.Particle;
import ass1.model.particle.ParticleSystem;

import java.util.List;
import java.util.function.Consumer;

public abstract class AbstractSimulationController<D> implements SimulationController {

    private final ParticleSystem<D> particleSystem;

    private int currentStep;
    private final int nSteps;
    private final double dt;
    private final Consumer<Long> simulationDelay;
    private Flag isRunning;

    private Thread simulationThread;

    protected AbstractSimulationController(final ParticleSystem<D> particleSystem,
                                           final int nSteps,
                                           final double dt,
                                           final Consumer<Long> simulationDelay)
    {
        this.particleSystem = particleSystem;
        this.nSteps = nSteps;
        this.dt = dt;
        this.simulationDelay = simulationDelay;
        this.currentStep = 0;
        this.isRunning = new Flag();
    }

    @Override
    public synchronized void startSimulation() {
        if(!isRunning.isSet()) {
            if(currentStep < nSteps) {
                simulationThread = new Thread(this::simulate);
                simulationThread.setName("SimulationController-Thread");
                isRunning.set();
                simulationThread.start();
            }
        }
    }

    @Override
    public synchronized void waitSimulation() {
        try {
            if(simulationThread != null)
                simulationThread.join();
        } catch (InterruptedException ignored) {}
    }

    @Override
    public synchronized void stopSimulation() {
        if(isRunning.isSet()) {
            isRunning.reset();
        }
    }

    private void simulate() {
        onSimulationStart();
        for (int i = currentStep; i < getSteps() && isRunning.isSet(); i++) {
            final long lastTime = System.currentTimeMillis();
            particleSystem.updateParticles(simulateSingleStep());
            final long realDt = System.currentTimeMillis() - lastTime;
            simulationDelay.accept(realDt);
            currentStep = i;
        }
        isRunning.reset();
        onSimulationEnd();
    }

    protected abstract void onSimulationStart();
    protected abstract void onSimulationEnd();
    public abstract List<Particle<D>> simulateSingleStep();

    protected int getSteps() { return nSteps; }
    protected ParticleSystem<D> getParticleSystem() { return particleSystem; }
    protected double getDeltaTime() { return dt; }
}
