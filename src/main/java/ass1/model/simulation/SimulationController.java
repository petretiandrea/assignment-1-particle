package ass1.model.simulation;

public interface SimulationController {
    void startSimulation();
    void waitSimulation();
    void stopSimulation();
}
