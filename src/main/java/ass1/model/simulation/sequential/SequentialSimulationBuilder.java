package ass1.model.simulation.sequential;

import ass1.model.particle.ParticleSystem;
import ass1.model.simulation.SimulationController;
import ass1.model.simulation.SimulationBuilder;

public class SequentialSimulationBuilder<D> extends SimulationBuilder<D> {

    public SequentialSimulationBuilder(ParticleSystem<D> particleSystem) {
        super(particleSystem);
    }

    @Override
    public SimulationController build() {
        return new SequentialSimulationController<>(
                getParticleSystem(),
                getSteps(),
                getDeltaTime(),
                getSimulationDelay()
        );
    }
}
