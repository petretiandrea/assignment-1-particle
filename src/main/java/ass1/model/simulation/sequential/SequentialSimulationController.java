package ass1.model.simulation.sequential;

import ass1.model.particle.Particle;
import ass1.model.particle.ParticleSystem;
import ass1.model.simulation.AbstractSimulationController;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class SequentialSimulationController<D> extends AbstractSimulationController<D> {

    private List<Particle<D>> modifiableList;

    private final int numberOfParticles;

    SequentialSimulationController(ParticleSystem<D> particleSystem, int nSteps, double dt, Consumer<Long> simulationDelay) {
        super(particleSystem, nSteps, dt, simulationDelay);
        modifiableList = new ArrayList<>(particleSystem.getCurrentParticles());
        numberOfParticles = particleSystem.getCurrentParticles().size();
    }

    @Override
    protected void onSimulationStart() {
        System.out.println("Sequential: SimulationController begin");
    }

    @Override
    protected void onSimulationEnd() {
        System.out.println("Sequential: SimulationController end");
    }

    @Override
    public List<Particle<D>> simulateSingleStep() {
        for(int i = 0; i < numberOfParticles; i++) {
            final Particle<D> particle = getParticleSystem().getCurrentParticles().get(i).deepCopy();
            final D force = getParticleSystem().computeForce(particle);
            particle.applyForce(force, getDeltaTime());
            modifiableList.set(i, particle);
        }
        return modifiableList;
    }
}
