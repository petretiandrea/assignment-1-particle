package ass1.model.simulation.parallel;

import ass1.model.particle.Particle;
import ass1.model.particle.ParticleSystem;
import ass1.model.simulation.AbstractSimulationController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

public class ParallelSimulationController<D> extends AbstractSimulationController<D> {

    private static final boolean DEBUG = false;

    private final List<Worker> workers;
    private final int numberOfParticles;
    private final Semaphore waitFinish = new Semaphore(0);
    private final int numberOfThreadAvailable;

    private List<Particle<D>> newComputation;

    ParallelSimulationController(ParticleSystem<D> particleSystem, int nSteps, double dt, Consumer<Long> simulationDelay, int numberOfThreads) {
        super(particleSystem, nSteps, dt, simulationDelay);
        numberOfThreadAvailable = numberOfThreads;
        numberOfParticles = particleSystem.getCurrentParticles().size();
        workers = new ArrayList<>();
        newComputation = new ArrayList<>(particleSystem.getCurrentParticles());
    }

    private void prepareWorkers(final int particleSize) {
        final int particlesForThread = particleSize / numberOfThreadAvailable;
        final int particlesExtra = particleSize % numberOfThreadAvailable;

        workers.clear();
        workers.addAll(IntStream.range(0, numberOfThreadAvailable)
                .mapToObj(i ->{
                    final boolean hasExtra = (i == numberOfThreadAvailable - 1);
                    final Worker w = new Worker(particlesForThread * i, particlesForThread + (hasExtra ? particlesExtra : 0), waitFinish);
                    w.setName("Worker-" + i);
                    return w;
                })
                .collect(Collectors.toList()));
    }

    protected void log(String message) {
        if(DEBUG) {
            synchronized (System.out) {
                System.out.println("[" + Thread.currentThread().getName() + "]: " + message);
            }
        }
    }

    @Override
    protected void onSimulationStart() {
        prepareWorkers(numberOfParticles);
        workers.forEach(Thread::start);
    }

    @Override
    public List<Particle<D>> simulateSingleStep() {
        log("Starting all computation");
        startAllComputation(workers);
        log("Waiting all computation to end");
        waitAllComputation(workers);
        log("All computation ends");
        return newComputation;
    }

    @Override
    protected void onSimulationEnd() {
        workers.forEach(Thread::interrupt);
        for (Worker w : workers) {
            try { w.join(); } catch (InterruptedException ignored) { }
        }
    }

    private void startAllComputation(List<Worker> workers) {
        workers.forEach(Worker::compute);
    }

    private void waitAllComputation(List<Worker> workers) {
        try {
            waitFinish.acquire(workers.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Worker extends Thread {

        private final Semaphore startComputeEvent;
        private final Semaphore finishEvent;
        private boolean isRunning;
        private final int from;
        private final int size;

        Worker(final int from, final int size, final Semaphore finishEvent) {
            this.from = from;
            this.size = size;
            this.startComputeEvent = new Semaphore(0);
            this.finishEvent = finishEvent;
        }

        void compute() {
            startComputeEvent.release();
        }

        @Override
        public void run() {
            isRunning = true;
            while (isRunning) {
                try {
                    log("Waiting for start");
                    startComputeEvent.acquire();
                    log("Stared");
                    for (int i = from; i < from + size; i++) {
                        final Particle<D> particle = getParticleSystem().getCurrentParticles().get(i).deepCopy();
                        particle.applyForce(getParticleSystem().computeForce(particle), getDeltaTime());
                        newComputation.set(i, particle);
                    }
                    log("Release a finish event");
                    finishEvent.release();
                } catch (InterruptedException ignored) {
                    isRunning = false;
                }
            }
        }
    }
}
