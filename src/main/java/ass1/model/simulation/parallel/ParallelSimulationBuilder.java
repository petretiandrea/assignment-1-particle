package ass1.model.simulation.parallel;

import ass1.model.particle.ParticleSystem;
import ass1.model.simulation.SimulationController;
import ass1.model.simulation.SimulationBuilder;

public class ParallelSimulationBuilder<D> extends SimulationBuilder<D> {

    private int numberOfThread;

    public ParallelSimulationBuilder(ParticleSystem<D> particleSystem) {
        super(particleSystem);
        numberOfThread = Runtime.getRuntime().availableProcessors();
    }

    public ParallelSimulationBuilder<D> setNumberOfThread(int numberOfThread) {
        this.numberOfThread = numberOfThread;
        return this;
    }

    @Override
    public SimulationController build() {
        return new ParallelSimulationController<>(
                getParticleSystem(),
                getSteps(),
                getDeltaTime(),
                getSimulationDelay(),
                numberOfThread
        );
    }
}
