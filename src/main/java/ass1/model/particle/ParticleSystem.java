package ass1.model.particle;

import ass1.model.computation.ForceComputationStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ParticleSystem<D> {

    private List<ParticleSystemObserver<D>> observers;
    private List<Particle<D>> particles;

    private ForceComputationStrategy<D> computationStrategy;
    private final double k;
    private final double kFriction;

    public ParticleSystem(List<Particle<D>> particles, ForceComputationStrategy<D> computationStrategy, double k, double kFriction) {
        this.particles = particles;
        this.computationStrategy = computationStrategy;
        this.k = k;
        this.kFriction = kFriction;
        this.observers = new ArrayList<>();
    }

    public void updateParticles(List<Particle<D>> particles) {
        this.particles.clear();
        this.particles.addAll(particles);
        notifyUpdate();
    }

    private synchronized void notifyUpdate() {
        observers.forEach(o -> o.onUpdateModel(this));
    }

    public List<Particle<D>> getCurrentParticles() {
        return particles;
    }

    public synchronized void attach(ParticleSystemObserver<D> observer) {
        if(!observers.contains(observer)) observers.add(observer);
    }

    public synchronized void detach(ParticleSystemObserver<D> observer) {
        observers.remove(observer);
    }

    public D computeForce(Particle<D> particle) {
        return computationStrategy.computeForce(particles, particle, k, kFriction);
    }

    public ParticleSystem<D> deepCopy() {
        return new ParticleSystem<>(
                particles.stream().map(Particle::deepCopy).collect(Collectors.toList()),
                computationStrategy,
                k,
                kFriction
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticleSystem<?> that = (ParticleSystem<?>) o;
        return Double.compare(that.k, k) == 0 &&
                Double.compare(that.kFriction, kFriction) == 0 &&
                Objects.equals(particles, that.particles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(particles, k, kFriction);
    }

    @Override
    public String toString() {
        return "ParticleSystem{" +
                "particles=" + particles +
                ", k=" + k +
                ", kFriction=" + kFriction +
                '}';
    }
}
