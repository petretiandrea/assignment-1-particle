package ass1.model.particle;

public class Vector3D {

    private double x;
    private double y;
    private double z;

    public static Vector3D zero() {
        return new Vector3D(0 ,0, 0);
    }

    public static Vector3D random() { return new Vector3D(Math.random(), Math.random(), Math.random()); }

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getMagnitude() {
        return Math.sqrt(x*x + y*y + z*z);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void setTo(double x, double y, double z) {
        setX(x);
        setY(y);
        setZ(z);
    }

    public Vector3D copy() {
        return new Vector3D(x, y, z);
    }
}
