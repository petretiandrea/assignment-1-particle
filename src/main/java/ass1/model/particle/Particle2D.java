package ass1.model.particle;

public class Particle2D extends Particle<Vector2D> {

    public static Particle<Vector2D> randomPosition(int id, double mass, double alpha) {
        return new Particle2D(id, Vector2D.random(), Vector2D.zero(), mass, alpha);
    }

    public Particle2D(int id, Vector2D position, Vector2D velocity, double mass, double alpha) {
        super(id, position, velocity, mass, alpha);
    }

    @Override
    public void applyForce(Vector2D force, double dt) {
        final double posX = getVelocity().getX() * dt;
        final double posY = getVelocity().getY() * dt;
        this.getPosition().setTo(this.getPosition().getX() + posX, this.getPosition().getY() + posY);

        final double velX = (force.getX() / getMass()) * dt;
        final double velY = (force.getY() / getMass()) * dt;
        this.getVelocity().setTo(this.getVelocity().getX() + velX, this.getVelocity().getY() + velY);
    }

    @Override
    public Particle<Vector2D> deepCopy() {
        return new Particle2D(getId(), getPosition().copy(), getVelocity().copy(), getMass(), getAlpha());
    }
}
