package ass1.model.particle;

import java.util.Objects;

public abstract class Particle<D> {

    private int id;
    private final D position;
    private final D velocity;
    private final double mass;
    private final double alpha;

    public Particle(int id, D position, D velocity, double mass, double alpha) {
        this.id = id;
        this.position = position;
        this.velocity = velocity;
        this.mass = mass;
        this.alpha = alpha;
    }


    public int getId() {
        return id;
    }
    public D getPosition() {
        return position;
    }
    public D getVelocity() { return velocity; }
    public double getMass() {
        return mass;
    }
    public double getAlpha() {
        return alpha;
    }

    public abstract void applyForce(final D force, double dt);
    public abstract Particle<D> deepCopy();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Particle particle = (Particle) o;
        return id == particle.id &&
                Double.compare(particle.mass, mass) == 0 &&
                Double.compare(particle.alpha, alpha) == 0 &&
                Objects.equals(position, particle.position) &&
                Objects.equals(velocity, particle.velocity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, position, velocity, mass, alpha);
    }

    @Override
    public String toString() {
        return "Particle{" +
                "id=" + id +
                ", position=" + position +
                ", velocity=" + velocity +
                ", mass=" + mass +
                ", alpha=" + alpha +
                '}';
    }
}
