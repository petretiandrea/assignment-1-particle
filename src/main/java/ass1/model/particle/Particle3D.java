package ass1.model.particle;

public class Particle3D extends Particle<Vector3D> {

    public Particle3D(int id, Vector3D position, Vector3D velocity, double mass, double alpha) {
        super(id, position, velocity, mass, alpha);
    }

    @Override
    public void applyForce(Vector3D force, double dt) {
        final double posX = getVelocity().getX() * dt;
        final double posY = getVelocity().getY() * dt;
        final double posZ = getVelocity().getZ() * dt;
        this.getPosition().setTo(this.getPosition().getX() + posX,
                this.getPosition().getY() + posY,
                this.getPosition().getZ() + posZ);

        final double velX = (force.getX() / getMass()) * dt;
        final double velY = (force.getY() / getMass()) * dt;
        final double velZ = (force.getZ() / getMass()) * dt;
        this.getVelocity().setTo(this.getVelocity().getX() + velX,
                this.getVelocity().getY() + velY,
                this.getVelocity().getZ() + velZ);
    }

    @Override
    public Particle<Vector3D> deepCopy() {
        return new Particle3D(getId(), getPosition().copy(), getPosition().copy(), getMass(), getAlpha());
    }
}
