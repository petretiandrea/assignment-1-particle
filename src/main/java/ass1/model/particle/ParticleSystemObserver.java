package ass1.model.particle;

public interface ParticleSystemObserver<D> {
    void onUpdateModel(ParticleSystem<D> particleSystem);
}
